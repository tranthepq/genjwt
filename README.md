# GenJwt

## Generate JWT token
Prepare claims file
```json
{
  "role": "admin"
}
```

Prepare rsa private key
```
# generate private key
openssl genrsa -out private-key.pem 4096
```

Generate token
```
go build

# with default values
./genjwt gen

# with custom input
./genjwt gen --private-key=<path-to-private-key> --claims=<path-to-custom-claims>
```

## Verify JWT token

```
# generate public key to verify token
openssl rsa -in private-key.pem -pubout -out public-key.pem

go build

./genjwt verify --public-key=<path-to-public-key> --token=<jwt-token-string>
```

## Build with goreleaser

[goreleaser quickstart](https://goreleaser.com/quick-start/)

Prerequisite:
- Git in clean state
- Have a tag (example v0.6) (tag will be used as release version)

### Dry run

Build only mode: `goreleaser build --rm-dist --snapshot`

Release without publish (require a Git tag): 

`goreleaser release --skip-publish --rm-dist`