package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"

	"github.com/spf13/cobra"
)

// genCmd represents the gen command
var genCmd = &cobra.Command{
	Use:   "gen",
	Short: "Generate jwt token with custom claims",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		privatePemFile := vipers["gen"].GetString("private-key")
		customClaimsFile := vipers["gen"].GetString("claims")

		log.Printf("👉 use private pem: %s\n", privatePemFile)
		log.Printf("👉 use custom claims: %s\n", customClaimsFile)

		payload, err := ioutil.ReadFile(customClaimsFile)
		if err != nil {
			log.Fatal("failed to read claims json file", err)
		}

		privateKey, err := ioutil.ReadFile(privatePemFile)
		if err != nil {
			log.Fatal("failed to read private pem file", err)
		}

		token, err := JwtToken(payload, privateKey)

		if err != nil {
			log.Fatal("failed to create jwt token")
		}

		log.Printf("🥳 token: %s\n", token)
	},
}

func JwtToken(payload, privKey []byte) (string, error) {
	key, err := jwt.ParseRSAPrivateKeyFromPEM(privKey)
	if err != nil {
		return "", err
	}

	// Map the claims from the payload.
	claims := new(jwt.MapClaims)
	if err := json.Unmarshal(payload, &claims); err != nil {
		return "", fmt.Errorf("error unmarshaling jwt claims: %w", err)
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	signed, err := token.SignedString(key)

	if err != nil {
		return "", err
	}

	return signed, nil
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["gen"] = v

	flags := genCmd.Flags()
	flags.String("private-key", "default/private-key.pem", "Private key PEM file to generate jwt token")
	flags.String("claims", "default/claims.json", "Custom claims file")

	if err := v.BindPFlags(flags); err != nil {
		log.Fatal(err)
	}

	rootCmd.AddCommand(genCmd)
}
