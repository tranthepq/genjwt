package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"

	"github.com/spf13/cobra"
)

// verifyCmd represents the verify command
var verifyCmd = &cobra.Command{
	Use:   "verify",
	Short: "Verify a JWT token with rsa public key",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		publicPemFile := vipers["verify"].GetString("public-key")
		token := vipers["verify"].GetString("token")

		log.Printf("👉 use public key pem: %s\n", publicPemFile)

		publicKey, err := ioutil.ReadFile(publicPemFile)
		if err != nil {
			log.Fatal("failed to read public pem file", err)
		}

		// verify token
		parseToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
			// convert to rsa key
			pub, _ := jwt.ParseRSAPublicKeyFromPEM(publicKey)

			return pub, nil
		})

		if parseToken == nil {
			log.Fatal("failed to parse token")
		}

		if parseToken.Valid {
			log.Printf("token is valid ✅")
			printJSON(parseToken)
		} else if ve, ok := err.(*jwt.ValidationError); ok {
			switch {
			case ve.Errors&jwt.ValidationErrorMalformed != 0:
				log.Fatal("token malformed")
			case ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0:
				log.Fatal("token expired, timing is everything")
			default:
				log.Fatal("couldn't handle the token", err)
			}
		} else {
			log.Fatal("couldn't handle the token", err)
		}
	},
}

func printJSON(j interface{}) {
	out, err := json.MarshalIndent(j, "", "    ")
	if err != nil {
		log.Fatal("failed to print jwt token", err)
	}

	fmt.Println(string(out))
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["verify"] = v

	flags := verifyCmd.Flags()
	flags.String("public-key", "default/public-key.pem", "Public key pem used to verify jwt token")
	flags.String("token", "", "Token to verify")

	if err := v.BindPFlags(flags); err != nil {
		log.Fatal(err)
	}

	rootCmd.AddCommand(verifyCmd)
}
